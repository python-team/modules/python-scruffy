python-scruffy (0.3.8.2-2) unstable; urgency=medium

  * Team upload.
  * Replace custom patch with python3-zombie-imp (Closes: #1081851)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 09 Dec 2024 11:31:12 +0100

python-scruffy (0.3.8.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.3.8.2.
  * Patch imp to importlib for Python 3.12 compatibility. (Closes: #1058129)
  * d/watch: Bump to version 4.
  * Bump standards version to 4.6.2
    - Update d/copyright to include upstream copyright notice.
    - d/control: Add Rules-Requires-Root field.
  * Remove unnecessary build dependencies
  * d/rules: Override dh_auto_test (upstream provides no test)
  * d/control: Add Testsuite field for basic autopkgtest
  * Patch setup.py for missing dependencies

 -- Dale Richards <dale@dalerichards.net>  Mon, 01 Jan 2024 15:20:03 +0000

python-scruffy (0.3.3-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:55:23 -0400

python-scruffy (0.3.3-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 20:58:55 +0200

python-scruffy (0.3.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add the missing dependencies on python{,3}-pkg-resources.
    (Closes: #896207, #896374)

 -- Adrian Bunk <bunk@debian.org>  Sun, 03 Jun 2018 21:53:39 +0300

python-scruffy (0.3.3-1) unstable; urgency=medium

  * Initial release. (Closes: #823992)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 11 May 2016 22:46:42 +0800
